package checkout;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import dataProvider.CheckOutLocators;

public class CheckOut extends CheckOutLocators
	{

	public String	ReqFirstName	= "First Name is required";
	public String	ReqLastName		= "Last Name is required";
	public String	ReqPostalCode	= "Postal Code is required";

	WebDriver driver;

	public CheckOut(WebDriver driver)
		{
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}

	public void FillingCheckOut(String Firstname, String Lastname, String Postalcode)
		{
			CheckOut.click();

			FirstName.sendKeys(Firstname);
			LastName.sendKeys(Lastname);
			PostalCode.sendKeys(Postalcode);

			CheckOut.click();
		}

	public void Clear()
		{
			FirstName.clear();
			LastName.clear();
			PostalCode.clear();
		}

	public String GetErrorMsg()
		{
			return ErrorMsg.getText();
		}
	}
