package checkout;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import cart.Cart;
import dataProvider.ConfigReader;
import dataProvider.Start;
import loginIn.Login_In;

public class Test_CheckOut
	{
	WebDriver		driver;
	ConfigReader	configReader;
	Login_In		loginobj;
	CheckOut		checkoutobj;
	Cart			cartobj;

	@BeforeMethod()
	public void StartUp()
		{

			configReader = new ConfigReader();

			driver = Start.BootStrap(configReader);

			checkoutobj = new CheckOut(driver);
			cartobj = new Cart(driver);
			loginobj = new Login_In(driver);

			loginobj.LoginIn(configReader.getLogin(), configReader.getPassword());
			cartobj.AddingToCart(1, 3);
			cartobj.ShoppingCart.click();
		}

	@Test(dataProvider = "Data")
	public void CheckOut(String Firstname, String Lastname, String Postalcode) throws InterruptedException
		{
			checkoutobj.FillingCheckOut(Firstname, Lastname, Postalcode);
			if (Firstname.equals("") || Lastname.equals("") || Postalcode.equals(""))
				{

					Assert.assertTrue(checkoutobj.GetErrorMsg().contains(checkoutobj.ReqFirstName)
							|| checkoutobj.GetErrorMsg().contains(checkoutobj.ReqLastName)
							|| checkoutobj.GetErrorMsg().contains(checkoutobj.ReqPostalCode));

					Assert.assertFalse(driver.getCurrentUrl().contains("www.saucedemo.com/checkout-step-two.html"));
				}
			else
				{
					Assert.assertTrue(driver.getCurrentUrl().contains("www.saucedemo.com/checkout-step-two.html"));
					driver.navigate().back();
				}

			checkoutobj.Clear();
		}

	@AfterMethod()
	public void Closing()
		{
			driver.close();
		}

	@DataProvider
	public Object[][] Data()
		{
			Object[][] data = new Object[8][3];

			// Correct login and password
			data[0][0] = "John";
			data[0][1] = "Kowalski";
			data[0][2] = "55-555";

			// Empty data
			data[1][0] = "";
			data[1][1] = "";
			data[1][2] = "";

			// No name
			data[2][0] = "";
			data[2][1] = "Kowalski";
			data[2][2] = "55-555";

			// No second name
			data[3][0] = "John";
			data[3][1] = "";
			data[3][2] = "55-555";

			// No zip code
			data[4][0] = "John";
			data[4][1] = "Kowalski";
			data[4][2] = "";

			// Only name
			data[5][0] = "John";
			data[5][1] = "";
			data[5][2] = "";

			// Only second name
			data[6][0] = "";
			data[6][1] = "Kowalski";
			data[6][2] = "";

			// Only zip code
			data[7][0] = "";
			data[7][1] = "";
			data[7][2] = "55-555";

			return data;
		}

	}
