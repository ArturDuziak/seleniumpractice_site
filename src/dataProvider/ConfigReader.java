package dataProvider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader
	{
	private Properties		properties;
	private final String	propertyFilePath	= "configs//Config.properties";

	public ConfigReader()
		{
			BufferedReader reader;
			try
				{
					reader = new BufferedReader(new FileReader(propertyFilePath));
					properties = new Properties();
					try
						{
							properties.load(reader);
							reader.close();
						}
					catch (IOException e)
						{
							e.printStackTrace();
						}
				}
			catch (FileNotFoundException e)
				{
					e.printStackTrace();
					throw new RuntimeException(
							"Config.properties haven't been found at this place: " + propertyFilePath);
				}
		}

	public String getDriverPath()
		{

			if (getBrowser().equals("chrome"))
				return properties.getProperty("Chromedriverpath");
			if (getBrowser().equals("gecko"))
				return properties.getProperty("FireFoxdriverpath");
			if (getBrowser().equals("edge"))
				return properties.getProperty("Edgedriverpath");
			else
				throw new RuntimeException("driverPath is missing from the config file");
		}

	public Integer getImplicitWait()
		{
			String implicitwait = properties.getProperty("ImplicitWait");
			if (implicitwait != null)
				return Integer.parseInt(implicitwait);
			else
				throw new RuntimeException("ImplicitWait is missing from the config file");
		}

	public String getUrl()
		{
			String url = properties.getProperty("url");
			if (url != null)
				return url;
			else
				throw new RuntimeException("Url is missing from the config file");
		}

	public String getLogin()
		{
			String login = properties.getProperty("login");
			if (login != null)
				return login;
			else
				throw new RuntimeException("Login is missing from the config file");
		}

	public String getPassword()
		{
			String password = properties.getProperty("password");
			if (password != null)
				return password;
			else
				throw new RuntimeException("Password is missing from the config file");
		}

	public String getBrowser()
		{
			String browser = properties.getProperty("browser");
			if (browser != null)
				return browser;
			else
				throw new RuntimeException("Browser is not chosen");

		}
	}
