package dataProvider;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginInLocators
	{

	@FindBy(css = "#user-name")
	protected WebElement	Username;
	@FindBy(css = "#password")
	protected WebElement	Password;
	@FindBy(css = ".btn_action")
	protected WebElement	LoginIn;
	@FindBy(css = "h3[data-test='error']")
	protected WebElement	ErrorMsg;

	}
