package dataProvider;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePageLocators
	{
	public String HomePageUrl = "https://www.saucedemo.com/inventory.html";

	@FindBy(css = ".bm-burger-button")
	public WebElement	Menu;
	@FindBy(css = "#inventory_sidebar_link")
	public WebElement	Menu_AllItems;
	@FindBy(css = "#about_sidebar_link")
	public WebElement	Menu_About;
	@FindBy(css = "#logout_sidebar_link")
	public WebElement	Menu_Logout;

	}
