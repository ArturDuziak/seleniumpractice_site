package dataProvider;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

public class CartLocators
	{
	@FindBy(css = "[class='fa-layers-counter shopping_cart_badge']")
	public WebElement		CartCount;
	@FindBy(css = "#shopping_cart_container")
	public WebElement		ShoppingCart;
	@FindBy(css = "[class='btn_secondary cart_button']")
	public WebElement		RemoveItem;
	@FindBy(xpath = "//a[@class='btn_secondary']")
	public WebElement		ContinueShopping;
	@FindAll(value = @FindBy(css = "[class='pricebar']>button"))
	public List<WebElement>	AddingToCart;
	@FindAll(value = @FindBy(css = ".inventory_item_price"))
	public List<WebElement>	AddingItemPrice;
	@FindAll(value = @FindBy(css = ".inventory_item_name"))
	public List<WebElement>	AddingItemName;
	}
