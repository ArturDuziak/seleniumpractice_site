package dataProvider;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Start
	{

	public static WebDriver BootStrap(ConfigReader configReader)
		{
			WebDriver driver = null;
			System.setProperty("webdriver." + configReader.getBrowser() + ".driver", configReader.getDriverPath());

			if (configReader.getBrowser().equals("chrome"))
				driver = new ChromeDriver();
			if (configReader.getBrowser().equals("gecko"))
				driver = new FirefoxDriver();
			if (configReader.getBrowser().equals("edge"))
				driver = new EdgeDriver();

			driver.manage().timeouts().implicitlyWait(configReader.getImplicitWait(), TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get(configReader.getUrl());

			return driver;
		}
	}
