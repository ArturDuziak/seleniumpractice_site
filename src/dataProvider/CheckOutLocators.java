package dataProvider;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckOutLocators
	{
	@FindBy(css = ".cart_checkout_link")
	public WebElement	CheckOut;
	@FindBy(xpath = "//*[@class='checkout_info']/form/input[1]")
	public WebElement	FirstName;
	@FindBy(xpath = "//*[@class='checkout_info']/form/input[2]")
	public WebElement	LastName;
	@FindBy(xpath = "//*[@class='checkout_info']/form/input[3]")
	public WebElement	PostalCode;
	@FindBy(css = "[value='CONTINUE']")
	public WebElement	Continue;
	@FindBy(css = "[data-test='error']")
	public WebElement	ErrorMsg;
	}
