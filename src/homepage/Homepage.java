package homepage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import dataProvider.HomePageLocators;

public class Homepage extends HomePageLocators
	{
	WebDriver driver;

	public Homepage(WebDriver driver)
		{
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}
	}
