package cart;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import dataProvider.CartLocators;

public class Cart extends CartLocators
	{
	WebDriver			driver;
	ArrayList<String>	NameList;
	ArrayList<Double>	PriceList;

	public Cart(WebDriver driver)
		{
			this.driver = driver;
			PageFactory.initElements(driver, this);

			NameList = new ArrayList<String>();
			PriceList = new ArrayList<Double>();
		}

	// Adds items to card starting for the first one up to the number you choose
	public void AddingToCart(int arg0, int... arg)
		{
			AddingToCart.get(arg0).click();

			for (int i = 0; i < arg.length; i++)
				{
					AddingToCart.get(arg[i]).click();
				}
		}

	public Double GetPrice(int k)
		{
			return Double.parseDouble(AddingItemPrice.get(k).getText().replace("$", ""));
		}

	public String GetName(int k)
		{
			return AddingItemName.get(k).getText();
		}

	public int ItemCount()
		{
			return AddingToCart.size();
		}

	public int CartCount()
		{
			return Integer.parseInt(CartCount.getText());
		}

	// Checks each item in card by their name and price (items from 1 to 6)
	public void CheckingCart()
		{
			ShoppingCart.click();

			for (int i = 0; i < NameList.size(); i++)
				{
					Assert.assertTrue(NameList.get(i).equals(GetName(i)) && PriceList.get(i).equals(GetPrice(i)));
				}
		}

	public void RemovingItemsCart()
		{
			for (int i = 1; i <= NameList.size(); i++)
				{
					RemoveItem.click();
				}
		}

	public void ItemList(int arg0, int... arg)
		{
			NameList.add(GetName(arg0));
			PriceList.add(GetPrice(arg0));

			for (int i = 0; i < arg.length; i++)
				{
					NameList.add(GetName(arg[i]));
					PriceList.add(GetPrice(arg[i]));
				}
		}
	}
