package cart;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import dataProvider.ConfigReader;
import dataProvider.Start;
import loginIn.Login_In;

public class Test_Cart
	{
	WebDriver		driver;
	ConfigReader	configReader;
	Cart			cartobj;
	Login_In		loginobj;

	@BeforeTest()
	public void StartUp()
		{

			configReader = new ConfigReader();
			driver = Start.BootStrap(configReader);

			loginobj = new Login_In(driver);

			loginobj.LoginIn(configReader.getLogin(), configReader.getPassword());
		}

	@Test(enabled = true)
	public void AddingToCartTest()
		{
			cartobj = new Cart(driver);

			cartobj.AddingToCart(1, 2, 4, 5, 3);
			cartobj.ItemList(1, 2, 4, 5, 3);

			Assert.assertTrue(cartobj.CartCount() == cartobj.NameList.size());

			cartobj.CheckingCart();
			cartobj.RemovingItemsCart();

			cartobj.ContinueShopping.click();

		}

	@Test(enabled = true)
	public void AddingToCartTest_AllItems()
		{
			cartobj = new Cart(driver);

			for (int i = 0; i < cartobj.ItemCount(); i++)
				{
					cartobj.AddingToCart(i);
					cartobj.ItemList(i);
				}
			Assert.assertTrue(cartobj.CartCount() == cartobj.ItemCount());

			cartobj.CheckingCart();
			cartobj.RemovingItemsCart();

			cartobj.ContinueShopping.click();
		}

	@AfterTest()
	public void Closing()
		{
			driver.quit();
		}
	}
