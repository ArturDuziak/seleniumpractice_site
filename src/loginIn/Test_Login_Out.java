package loginIn;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import dataProvider.ConfigReader;
import dataProvider.Start;

public class Test_Login_Out
	{
	WebDriver		driver;
	ConfigReader	configReader;
	Login_In		loginobj_in;
	Login_Out		loginobj_out;

	@BeforeTest()
	public void StartUp()
		{
			configReader = new ConfigReader();

			driver = Start.BootStrap(configReader);
		}

	@Test
	public void LoginOut() throws InterruptedException
		{
			loginobj_in = new Login_In(driver);
			loginobj_out = new Login_Out(driver);

			loginobj_in.LoginIn(configReader.getLogin(), configReader.getPassword());

			loginobj_out.LoginOut();

			Assert.assertTrue(driver.getCurrentUrl().equals(configReader.getUrl()));
		}

	}
