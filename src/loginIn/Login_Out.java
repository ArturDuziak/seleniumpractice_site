package loginIn;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import dataProvider.HomePageLocators;

public class Login_Out extends HomePageLocators
	{
	WebDriver driver;

	public Login_Out(WebDriver driver)
		{
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}

	public void LoginOut() throws InterruptedException
		{
			Menu.click();
			Thread.sleep(1000);

			Menu_Logout.click();
		}
	}
