package loginIn;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import dataProvider.LoginInLocators;

public class Login_In extends LoginInLocators
	{

	WebDriver driver;

	public String	ReqUsername	= "Username is required";
	public String	ReqPassword	= "Password is required";
	public String	NoMatch		= "Username and password do not match any user in this service";
	public String	LockedOut	= "this user has been locked out";

	public Login_In(WebDriver driver)
		{
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}

	public void LoginIn(String username, String password)
		{
			this.SetUsername(username);
			this.SetPassword(password);
			this.ClickOnLogin();
		}

	private void SetUsername(String username)
		{
			Username.sendKeys(username);
		}

	private void SetPassword(String password)
		{
			Password.sendKeys(password);
		}

	private void ClickOnLogin()
		{
			LoginIn.click();
		}

	public String GetErrorMsg()
		{
			return ErrorMsg.getText();
		}
	}
