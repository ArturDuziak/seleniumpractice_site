package loginIn;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import dataProvider.ConfigReader;
import dataProvider.HomePageLocators;
import dataProvider.Start;

public class Test_Login_In
	{
	private WebDriver	driver;
	Login_In			LoginObj;
	ConfigReader		configReader;
	HomePageLocators	HomePageObj;

	@BeforeMethod()
	private void StartUp()
		{
			configReader = new ConfigReader();

			driver = Start.BootStrap(configReader);
		}

	@Test(dataProvider = "Data")
	private void LoginInTest(String username, String password) throws InterruptedException
		{
			HomePageObj = new HomePageLocators();
			LoginObj = new Login_In(driver);

			LoginObj.LoginIn(username, password);

			if (username.equals("") || password.equals(""))
				{
					Assert.assertTrue(LoginObj.GetErrorMsg().contains(LoginObj.ReqUsername)
							|| LoginObj.GetErrorMsg().contains(LoginObj.ReqPassword));
				}

			if (username.equals("locked_out_user"))
				{
					Assert.assertTrue(LoginObj.GetErrorMsg().contains(LoginObj.LockedOut));
				}

			if (username.equals(configReader.getLogin()) && password.equals(configReader.getPassword()))
				{
					Assert.assertTrue(driver.getCurrentUrl().equals(HomePageObj.HomePageUrl));
				}
		}

	@AfterMethod()
	private void Closing()
		{
			driver.close();
		}

	@DataProvider
	public Object[][] Data()
		{
			configReader = new ConfigReader();

			Object[][] data = new Object[5][2];

			// Correct data
			data[0][0] = configReader.getLogin();
			data[0][1] = configReader.getPassword();

			// Empty data
			data[1][0] = "";
			data[1][1] = "";

			// Good login correct password
			data[2][0] = configReader.getLogin();
			data[2][1] = "";

			// No login correct password
			data[3][0] = "";
			data[3][1] = configReader.getPassword();

			// Locked_out_user
			data[4][0] = "locked_out_user";
			data[4][1] = configReader.getPassword();

			return data;
		}

	}
